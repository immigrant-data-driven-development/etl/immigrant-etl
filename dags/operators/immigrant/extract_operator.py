
from airflow.models import BaseOperator
from airflow.models.taskinstance import Context
from airflow.providers.mongo.hooks.mongo import MongoHook
import json

class ExtractOperator(BaseOperator):
    def __init__(self,collection_name:str, **kwargs):
        super().__init__(**kwargs)
        
        self.hook = MongoHook(conn_id='mongo_default')
        self.client = self.hook.get_conn()
        self.collection_name = collection_name
        try:            
            print (self.client.server_info())
        
        except Exception as e:
            print (e, "Unable to connect to be server")
        
    def save(self, data):
        database = self.client["msdevops"]
        collection = database[self.collection_name]
        item_list = []
        
        for item in data:
            item_list.append (item.__dict__)

        collection.insert_many(item_list)