from __future__ import annotations

from airflow import DAG
from datetime import datetime, timedelta
from airflow.operators.bash import BashOperator
from operators.immigrant.msdevops_operator import (MSDevOpsProjectOperator,MSDevOpsTeamOperator,MSDevOpsInteractionOperator,MSDevOpsTeamMemberOperator,MSDevOpsWorkitemOperator)


organization_url = "https://dev.azure.com/recomb/"
personal_access_token = "bwqwzmrvvbzo4ol56l2crmczreexsz6nnjf2u3x4tqy7fgagvyzq"

with DAG(

    'extract_ms_devops',
    default_args={
        'depends_on_past': False,
        'email': ['paulossjunior@gmail.com'],
        'email_on_failure': True,
        'email_on_retry': True,                
    },
    
    description='Extract data from DevOps',
    schedule=timedelta(days=1),
    start_date=datetime(2021, 1, 1),
    catchup=False,
    tags=['extract','MS DevOps','Immigrant'],
) as dag:
    
    start = BashOperator(task_id='start',bash_command='date',)

    project = MSDevOpsProjectOperator(task_id='project',organization_url=organization_url, personal_access_token=personal_access_token,)

    team = MSDevOpsTeamOperator(task_id='team',organization_url=organization_url, personal_access_token=personal_access_token,)

    interaction = MSDevOpsInteractionOperator(task_id='interaction',organization_url=organization_url, personal_access_token=personal_access_token,)
    
    teammember = MSDevOpsTeamMemberOperator(task_id='teammember',organization_url=organization_url, personal_access_token=personal_access_token,)
    
    workitem = MSDevOpsWorkitemOperator(task_id='workitem',organization_url=organization_url, personal_access_token=personal_access_token,)
    
    end = BashOperator(task_id='end',bash_command='date',)
    
    # start >> [project,team, interaction, teammember, workitem] >> end

    # 

    

    

    
