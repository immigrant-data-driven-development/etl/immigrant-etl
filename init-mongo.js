db.createUser(
    {
        user:"YourUserName",
        pwd:"YourPasswordName",
        roles: [
            {
                role: "readwrite",
                db: "your-database-name"
            }
        ]
    }
)